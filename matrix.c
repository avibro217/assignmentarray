#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define N 2


void matrixmul(int mat1[][N], int mat2[][N], int mat3[][N]) 
{ 
    int i, j, k; 
    for (i = 0; i < N; i++) 
    { 
        for (j = 0; j < N; j++) 
        { 
            mat3[i][j] = 0; 
            for (k = 0; k < N; k++) 
                mat3[i][j] += mat1[i][k]*mat2[k][j]; 
        } 
    } 
} 

	int main(){
	//input data
	int rows,columns;
	puts("Enter the number of rows");
	scanf("%d",&rows);
	puts("Enter the number of columns");
	scanf("%d",&columns);
	
	
	//define matrices
	int matrix1[rows][columns];
	int matrix2 [rows][columns];
	int matrix3 [rows][columns];
	
	//define forloop variables
	int i,j;
	
	//define the elements in the matrices
	
	//matrix1
	puts("Enter the elements for matrix 1 ");
	for(i=0;i<rows;i++)
	{
		for(j=0;j<columns;j++)
		{
			scanf("%d",&matrix1[i][j]);	
		}
	}
	
	
	
	
		//matrix2
		puts("Enter the elements for matrix 2 ");
	for(i=0;i<rows;i++)
	{
		for(j=0;j<columns;j++)
		{
			scanf("%d",&matrix2[i][j]);	
		}
	}
	
	printf("\n");
	printf("\n");
	printf("\n");
	
	// print matrix1
	puts("This is matrix 1");
	for(i=0;i<rows;i++)
	{
		for(j=0;j<columns;j++)
		{
			printf("%3d",matrix1[i][j]);	
		}
		printf("\n");
	}
	
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	
	
		// print matrix2
		puts("This is matrix 2");
	for(i=0;i<rows;i++)
	{
		for(j=0;j<columns;j++)
		{
			printf("%3d",matrix2[i][j]);	
		}
		printf("\n");
	}
	
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	
	//addition of the two matrices
	puts("This is the addition of matrix 1 and matrix 2");
	for(i=0;i<rows;i++)
	{
		for(j=0;j<columns;j++)
		{
			matrix3[i][j] = matrix1[i][j]+matrix2[i][j];
			printf("%3d",matrix3[i][j]);
		
		}
			printf("\n");
	
	}
	

//multiplication of the two matrices


   matrixmul(matrix1, matrix2, matrix3); 
  
    printf("Result matrix is \n"); 
    for (i = 0; i < N; i++) 
    { 
        for (j = 0; j < N; j++) 
           printf("   %2d ", matrix3[i][j]); 
        printf("\n"); 
    } 



	
	
	return 0;
	}