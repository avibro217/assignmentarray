//**********************find the frequency of a given character in a given string*******************



#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define SIZE 50


int main(){

	char string[SIZE];
	char character;
	puts(":::Enter the sentence:::");
	fgets(string,SIZE,stdin);
	puts(":::Enter the character , of which frequency needed:::");
	scanf("%c",&character);
	int i = 0;
	int count =0;
	while(string[i] != '\n')
	{
		if(string[i] == character)
		{
			count++;
		}
		
		i++;
	}
	printf("The frequency of the character \'%c\' in the given sentence is %d",character,count);
	


return 0;
}